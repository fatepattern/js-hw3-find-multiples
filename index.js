let m;
let n;

alert("Enter your range of numbers");

while(m === null || isNaN(m) || m === '' || Number.isInteger(m) === 'false'){
    m = prompt("Enter your lower number");
}

while(n === null || isNaN(n) || n === '' || n < m || Number.isInteger(n) === 'false'){
    n = prompt("What is your upper number");
}

let numbersArr = [];

for(let i = m; i <= n; i++){
    if(i%5 === 0){
        numbersArr.push(i);
    } 
}

for(let i = 0; i < numbersArr.length; i++){
    console.log(numbersArr[i]);
}

if(numbersArr.length === 0){
    console.log("Sorry, no numbers");
}

